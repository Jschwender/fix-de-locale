#!/bin/bash
#
# Ersetzt die Datumsformate der locale der veralteten Form nach DIN 1355-1 durch die (1996) neue Form nach ISO 8601
# und setzt den korrekten Tausendertrenner als schmales geschützes Leerzeichen.
#
PFAD=/usr/share/i18n/locales
while read XXX
do
    if [ -f $XXX.sav ]
    then {
	echo "$XXX.sav		exisitert bereits! Kein Überschreiben."
    } else {
#	sed -e "s/d_fmt *\"%d.%m.%Y\"/d_fmt \"%Y-%m-%d\"/g"  -e "s/d_t_fmt *\"%a %d %b %Y %T %Z\"/d_t_fmt \"%Y-%m-%dT%T %Z\"/g"  $XXX
	sed --in-place=.sav -e "s/d_fmt *\"%d.%m.%Y\"/d_fmt \"%Y-%m-%d\"/g"  \
          -e "s/d_t_fmt *\"%a %d %b %Y %T %Z\"/d_t_fmt \"%Y-%m-%dT%T %Z\"/g" \
          -e "s/^thousands_sep .*/thousands_sep \"\<U202F\>\"/g" \
            $XXX
    }
    fi
    done < <( find $PFAD -type f -name "de_DE"|grep -v '.sav' )
#
lsb_release -i -s|grep -s "[Ubuntu|Debian]" {
# Überschreibt die Liste gültiger locale um die meist nicht benötigten zu eliminieren
#
cat <<EOF > /var/lib/locales/supported.d/de
de_DE.UTF-8 UTF-8
de_CH.UTF-8 UTF-8
de_AT.UTF-8 UTF-8
EOF

cat <<EOF > /var/lib/locales/supported.d/en
en_DK.UTF-8 UTF-8
en_US.UTF-8 UTF-8
en_GB.UTF-8 UTF-8
EOF
}

locale-gen
