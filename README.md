# Datumsschreibweise de_DE

- [ ] 1943 legt die DIN 1355 zum ersten mal die Schreibweise DD.MM.YY fest.
- [ ] 1975 folgte die 2. Ausgabe als DIN 1355-1.
- [ ] 1988 begann ein ISO Gremium mit der Ausarbeitung einer Norm für den internationalen Austausch.
- [ ] 1994 erschien die EN 28601 und die DIN 1355 wurde zurückgezogen.
- [ ] 2004 wurde die ISO 8601 als DIN ISO 8601 in die Normierung aufgenommen.

Die DIN 5008 regelt im deutschen Sprachraum die Schreibweise des Datums in Text- und Informationsverarbeitung.
Auch hier wird auf die Schreibweise nach ISO 8601 verweisen, mit einer Ausnahme für die DIN 1355 Schreibweise "wenn keine Missverständnisse zu erwarten sind".
D.h. wer davon ausgeht dass jeder Kommunikationspartner mit tradierten deutschen kulturellen Gepflogenheiten wie der 
veralteten DIN 1355 vertraut ist kann die TT.MM.JJ Schreibweise weiter führen.

## Linux lokale Formate

In Linux ist die lokalisierung verschiedener Einstellungen zentral durch "locales", ein Teil der GNU C Biblithek zusammengefasst und offensichtlich von der FSF gepflegt.
In /usr/share/i18n/locales steht für jeden Sprach-Ländercode eine eigene Konfigurationsdatei zur Verfügung.
Die FSF ignoriert wohl bis heute die geltende Norm und verwendet nach wie vor die DIN 1355 Schreibweise aus dem 3. Reich.


## Abhilfe

Ein kleines Skript ändert die Datei für de_DE einfach indem sie als root aufgerufen wird.